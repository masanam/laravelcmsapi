<!DOCTYPE html>
<html lang="en">
  <head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@NusaHub">
    <meta name="twitter:creator" content="@NusaHub">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="DashForge">
    <meta name="twitter:description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="twitter:image" content="http://www.nusahub.co.id/ ">

    <!-- Facebook -->
    <meta property="og:url" content="http://www.nusahub.co.id/">
    <meta property="og:title" content="DashForge">
    <meta property="og:description" content="Responsive Bootstrap 4 Dashboard Template">

    <meta property="og:image" content="http://www.nusahub.co.id/">
    <meta property="og:image:secure_url" content="http://www.nusahub.co.id/ ">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="author" content="NusaHub">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('vendor/dashforge/assets/img/favicon.png')); ?>">

    <title>PT.NusaHub</title>

    <!-- vendor css -->
    <link href="<?php echo e(asset('vendor/dashforge/lib/@fortawesome/fontawesome-free/css/all.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('vendor/dashforge/lib/ionicons/css/ionicons.min.css')); ?>" rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('vendor/dashforge/assets/css/dashforge.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('vendor/dashforge/assets/css/dashforge.auth.css')); ?>">
  </head>
  <body>

    <header class="navbar navbar-header navbar-header-fixed">
      
      <div class="navbar-brand">
        <img src="<?php echo e(asset('img/logo.png')); ?>" alt="logo-fintech" style="width:150px;">
      </div><!-- navbar-brand -->
      
    </header><!-- navbar -->

    <div class="content content-fixed content-auth">
      <div class="container">
        <div class="media align-items-stretch justify-content-center ht-100p pos-relative">
          <div class="media-body align-items-center d-none d-lg-flex">
          <div class="mx-wd-600">
              <img src="<?php echo e(asset('img/nusahub.png')); ?>" class="img-fluid" alt="">
            </div>
            <div class="pos-absolute b-0 l-0 tx-12 tx-center">
            </div>
          </div><!-- media-body -->
          <form class="sign-wrapper mg-lg-l-50 mg-xl-l-60" method="post" action="<?php echo e(url('/login')); ?>">
            <?php echo csrf_field(); ?>

          <!-- <div class="sign-wrapper mg-lg-l-50 mg-xl-l-60"> -->
            <div class="wd-100p">
              <h3 class="tx-color-01 mg-b-5">Sign In</h3>
              <p class="tx-color-03 tx-16 mg-b-40">Welcome back! Please signin to continue.</p>

              <div class="form-group">
                <label>Email address</label>
                <input type="email" name="email" class="form-control" placeholder="yourname@yourmail.com">
                <?php if($errors->has('email')): ?>
                  <span class="help-block">
                    <strong><?php echo e($errors->first('email')); ?></strong>
                  </span>
                <?php endif; ?>
              </div>
              <div class="form-group">
                <div class="d-flex justify-content-between mg-b-5">
                  <label class="mg-b-0-f">Password</label>
                  <a href="<?php echo e(url('/password/reset')); ?>" class="tx-13">Forgot password?</a>
                </div>
                <input type="password" name="password" class="form-control" placeholder="Enter your password">
                <?php if($errors->has('password')): ?>
                  <span class="help-block">
                    <strong><?php echo e($errors->first('password')); ?></strong>
                  </span>
                <?php endif; ?>
              </div>
              <button class="btn btn-brand-02 btn-block">Sign In</button>
              <div class="divider-text">or</div>
              <button class="btn btn-outline-facebook btn-block">Sign In With Facebook</button>
              <button class="btn btn-outline-twitter btn-block">Sign In With Twitter</button>
              <div class="tx-13 mg-t-20 tx-center">Don't have an account? <a href="<?php echo e(url('/register')); ?>">Create an Account</a></div>
            </div>
          <!-- </div> -->
          </form><!-- sign-wrapper -->
        </div><!-- media -->
      </div><!-- container -->
    </div><!-- content -->

    <footer class="footer">
      <div>
        <span>&copy; 2020 PT.NusaHub v1.1.1. </span>
        <span>Created by <a href="http://www.nusahub.co.id">PT.NusaHub</a></span>
      </div>
      <div>
        <nav class="nav">
          <a href="#" class="nav-link">Licenses</a>
          <a href="#" class="nav-link">Change Log</a>
          <a href="#" class="nav-link">Get Help</a>
        </nav>
      </div>
    </footer>

    <script src="<?php echo e(asset('vendor/dashforge/lib/jquery/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('vendor/dashforge/lib/bootstrap/js/bootstrap.bundle.min.js')); ?>"></script>
    <script src="<?php echo e(asset('vendor/dashforge/lib/feather-icons/feather.min.js')); ?>"></script>
    <script src="<?php echo e(asset('vendor/dashforge/lib/perfect-scrollbar/perfect-scrollbar.min.js')); ?>"></script>

    <script src="<?php echo e(asset('vendor/dashforge/assets/js/dashforge.js')); ?>"></script>

    <!-- append theme customizer -->
    <script src="<?php echo e(asset('vendor/dashforge/lib/js-cookie/js.cookie.js')); ?>"></script>
    

  </body>
</html>
<?php /**PATH /Users/choirulanam/php/nusahub/LaravelCMSAPI/resources/views/auth/dashforge-templates/login.blade.php ENDPATH**/ ?>