<?php $__env->startSection('contents'); ?>
    <div class="content">
        <div class="container">
            <?php echo $__env->make('flash::message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Sections</li>
                        </ol>
                    </nav>
                    <!-- <h1 class="mg-b-0 tx-spacing--1">Sections</h1> -->
                </div>

                <div class="d-none d-md-block">
                    
                    <a class="btn btn-sm btn-primary btn-uppercase" href="<?php echo route('admin.sections.create'); ?>"><i class="fa fa-plus"></i> Add New</a>
                </div>
            </div>

            <h4 class="mg-b-10">Sections</h4>

            <div data-label="List" class="df-example demo-forms">

            <div class="table table-striped table-responsive">
                <?php echo $__env->make('admin.sections.table', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
            </div>

        </div>
    </div>
    <!-- /.content -->

    <!-- Modal -->
    
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/choirulanam/php/nusahub/LaravelCMSAPI/resources/views/admin/sections/index.blade.php ENDPATH**/ ?>