<?php $__env->startSection('contents'); ?>
    
    <div class="content">
        <div class="container">
            <?php echo $__env->make('dashforge-templates::common.errors', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

            <h4 id="section1" class="mg-b-10">Slider</h4>

            <p class="mg-b-30">Please, fill all required fields before click save button.</p>

            <div data-label="Create" class="df-example demo-forms sliders-forms">
                <?php echo Form::open(['route' => 'admin.sliders.store']); ?>

                    <?php echo $__env->make('admin.sliders.fields', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <?php echo Form::close(); ?>

            </div>
        </div>
    </div>
    <!-- /.content -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/choirulanam/php/nusahub/LaravelCMSAPI/resources/views/admin/sliders/create.blade.php ENDPATH**/ ?>