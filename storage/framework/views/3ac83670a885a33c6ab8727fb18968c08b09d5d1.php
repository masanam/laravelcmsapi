<li class="nav-label">Dashboard</li>
<li class="<?php echo e(Request::is('stats*') ? 'active' : ''); ?> nav-item">
    <a class="nav-link" href="<?php echo url('stats'); ?>"><i data-feather="activity"></i><span>Stats</span></a>
</li>

<?php if(Auth::user()->is_admin || Auth::user()->tenancy_id): ?>

<?php endif; ?>

<li class="nav-label mg-t-25">Setting</li>
<li class="<?php echo e(Request::is('sliders*') ? 'active' : ''); ?> nav-item">
    <a class="nav-link" href="<?php echo route('admin.sliders.index'); ?>"><i data-feather="edit"></i><span>Sliders</span></a>
</li>


<li class="nav-label mg-t-25">Articles</li>
<li class="<?php echo e(Request::is('categories*') ? 'active' : ''); ?> nav-item">
    <a class="nav-link" href="<?php echo route('admin.categories.index'); ?>"><i data-feather="edit"></i><span>Categories</span></a>
</li>
<li class="<?php echo e(Request::is('contents*') ? 'active' : ''); ?> nav-item">
    <a class="nav-link" href="<?php echo route('admin.contents.index'); ?>"><i data-feather="edit"></i><span>Articles</span></a>
</li>

<li class="nav-label mg-t-25">Documents Download</li>
<li class="<?php echo e(Request::is('jenis*') ? 'active' : ''); ?> nav-item">
    <a class="nav-link" href="<?php echo route('admin.jenis.index'); ?>"><i data-feather="edit"></i><span>Jenis Documents</span></a>
</li>

<li class="<?php echo e(Request::is('documents*') ? 'active' : ''); ?> nav-item">
    <a class="nav-link" href="<?php echo route('admin.documents.index'); ?>"><i data-feather="edit"></i><span>Documents</span></a>
</li>



<li class="nav-label mg-t-25">Form Submit</li>
<li class="<?php echo e(Request::is('contacts*') ? 'active' : ''); ?> nav-item">
    <a class="nav-link" href="<?php echo route('admin.contacts.index'); ?>"><i data-feather="edit"></i><span>Contacts Us</span></a>
</li>

<li class="<?php echo e(Request::is('investors*') ? 'active' : ''); ?> nav-item">
    <a class="nav-link" href="<?php echo route('admin.investors.index'); ?>"><i data-feather="edit"></i><span>Investors</span></a>
</li>

<li class="<?php echo e(Request::is('registrants*') ? 'active' : ''); ?> nav-item">
    <a class="nav-link" href="<?php echo route('admin.registrants.index'); ?>"><i data-feather="edit"></i><span>Registrants</span></a>
</li>


<li class="nav-label mg-t-25">Default</li>
<li class="<?php echo e(Request::is('menus*') ? 'active' : ''); ?> nav-item">
    <a class="nav-link" href="<?php echo route('admin.menus.index'); ?>"><i data-feather="edit"></i><span>Menus</span></a>
</li>

<li class="<?php echo e(Request::is('statuses*') ? 'active' : ''); ?> nav-item">
    <a class="nav-link" href="<?php echo route('admin.statuses.index'); ?>"><i data-feather="edit"></i><span>Statuses</span></a>
</li>

<li class="<?php echo e(Request::is('parts*') ? 'active' : ''); ?> nav-item">
    <a class="nav-link" href="<?php echo route('admin.parts.index'); ?>"><i data-feather="edit"></i><span>Type Sections</span></a>
</li>


<li class="<?php echo e(Request::is('sections*') ? 'active' : ''); ?> nav-item">
    <a class="nav-link" href="<?php echo route('admin.sections.index'); ?>"><i data-feather="edit"></i><span>Sections</span></a>
</li>

<li class="<?php echo e(Request::is('distributions*') ? 'active' : ''); ?> nav-item">
    <a class="nav-link" href="<?php echo route('admin.distributions.index'); ?>"><i data-feather="edit"></i><span>Distributions</span></a>
</li>

<li class="nav-label mg-t-25">User Management</li>
<li class="<?php echo e(Request::is('roles*') ? 'active' : ''); ?> nav-item">
    <a class="nav-link" href="<?php echo route('admin.roles.index'); ?>"><i data-feather="edit"></i><span>Roles</span></a>
</li>

<li class="<?php echo e(Request::is('users*') ? 'active' : ''); ?> nav-item">
    <a class="nav-link" href="<?php echo route('admin.users.index'); ?>"><i data-feather="edit"></i><span>Users</span></a>
</li>

<li class="<?php echo e(Request::is('permissions*') ? 'active' : ''); ?> nav-item">
    <a class="nav-link" href="<?php echo route('admin.permissions.index'); ?>"><i data-feather="edit"></i><span>Permissions</span></a>
</li>




<?php /**PATH /Users/choirulanam/php/nusahub/LaravelCMSAPI/resources/views/layouts/menu.blade.php ENDPATH**/ ?>