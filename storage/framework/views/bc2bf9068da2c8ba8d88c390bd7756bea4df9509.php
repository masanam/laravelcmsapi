<!DOCTYPE html>
<html lang="en">
  <head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@NusaHub">
    <meta name="twitter:creator" content="@NusaHub">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="DashForge">
    <meta name="twitter:description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="twitter:image" content="http://www.nusahub.co.id/ ">

    <!-- Facebook -->
    <meta property="og:url" content="http://www.nusahub.co.id/dashforge">
    <meta property="og:title" content="DashForge">
    <meta property="og:description" content="Responsive Bootstrap 4 Dashboard Template">

    <meta property="og:image" content="http://www.nusahub.co.id/ ">
    <meta property="og:image:secure_url" content="http://www.nusahub.co.id/ ">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="author" content="NusaHub">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('vendor/dashforge/assets/img/favicon.png')); ?>">

    <title>webcore</title>

    <!-- vendor css -->
    <link href="<?php echo e(asset('vendor/dashforge/lib/@fortawesome/fontawesome-free/css/all.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('vendor/dashforge/lib/ionicons/css/ionicons.min.css')); ?>" rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('vendor/dashforge/assets/css/dashforge.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('vendor/dashforge/assets/css/dashforge.auth.css')); ?>">
  </head>
  <body>

    <header class="navbar navbar-header navbar-header-fixed">
      
      <div class="navbar-brand">
        <img src="<?php echo e(asset('img/logo.png')); ?>" alt="logo-fintech" style="width:150px;">
      </div><!-- navbar-brand -->
      
    </header><!-- navbar -->

    <div class="content content-fixed content-auth">
      <div class="container">
        <div class="media align-items-stretch justify-content-center ht-100p">
          <form class="sign-wrapper mg-lg-r-50 mg-xl-r-60" method="post" action="<?php echo e(url('/register')); ?>">
            <?php echo csrf_field(); ?>

          <!-- <div class="sign-wrapper mg-lg-r-50 mg-xl-r-60"> -->
            <div class="pd-t-20 wd-100p">
              <h4 class="tx-color-01 mg-b-5">Create New Account</h4>
              <p class="tx-color-03 tx-16 mg-b-40">It's free to signup and only takes a minute.</p>

              <div class="form-group">
                <label>Name</label>
                <input type="text" name="name" class="form-control<?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" placeholder="Enter your firstname">

                <?php if($errors->has('name')): ?>
                    <span class="help-block text-danger">
                        <strong><?php echo e($errors->first('name')); ?></strong>
                    </span>
                <?php endif; ?>
              </div>
              <div class="form-group">
                <label>Email address</label>
                <input type="email" name="email" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" placeholder="Enter your email address">

                <?php if($errors->has('email')): ?>
                    <span class="help-block text-danger">
                        <strong><?php echo e($errors->first('email')); ?></strong>
                    </span>
                <?php endif; ?>
              </div>
              <div class="form-group">
                <div class="d-flex justify-content-between mg-b-5">
                  <label class="mg-b-0-f">Password</label>
                </div>
                <input type="password" name="password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" placeholder="Enter your password">

                <?php if($errors->has('password')): ?>
                    <span class="help-block text-danger">
                        <strong><?php echo e($errors->first('password')); ?></strong>
                    </span>
                <?php endif; ?>
              </div>
              <div class="form-group">
                <div class="d-flex justify-content-between mg-b-5">
                  <label class="mg-b-0-f">Confirm Password</label>
                </div>
                <input type="password" name="password_confirmation" class="form-control<?php echo e($errors->has('password_confirmation') ? ' is-invalid' : ''); ?>" placeholder="Enter to confirm your password">

                <?php if($errors->has('password_confirmation')): ?>
                    <span class="help-block text-danger">
                        <strong><?php echo e($errors->first('password_confirmation')); ?></strong>
                    </span>
                <?php endif; ?>
              </div>
              
              <div class="form-group tx-12">
                By clicking <strong>Create an account</strong> below, you agree to our terms of service and privacy statement.
              </div><!-- form-group -->

              <button class="btn btn-brand-02 btn-block">Create Account</button>
              <div class="divider-text">or</div>
              <button class="btn btn-outline-facebook btn-block">Sign Up With Facebook</button>
              <button class="btn btn-outline-twitter btn-block">Sign Up With Twitter</button>
              <div class="tx-13 mg-t-20 tx-center">Already have an account? <a href="<?php echo e(url('/login')); ?>">Sign In</a></div>
            </div>
          <!-- </div> -->
          </form><!-- sign-wrapper -->
          <div class="media-body pd-y-30 pd-lg-x-50 pd-xl-x-60 align-items-center d-none d-lg-flex pos-relative">
          <div class="mx-wd-600">
              <img src="<?php echo e(asset('img/nusahub.png')); ?>" class="img-fluid" alt="">
            </div>
            <div class="pos-absolute b-0 r-0 tx-12">
            </div>
          </div><!-- media-body -->
        </div><!-- media -->
      </div><!-- container -->
    </div><!-- content -->

    <footer class="footer">
      <div>
        <span>&copy; 2020 webcore v1.1.1. </span>
        <span>Created by <a href="http://www.nusahub.co.id">webcore</a></span>
      </div>
      <div>
        <nav class="nav">
          <a href="https://themeforest.net/licenses/standard" class="nav-link">Licenses</a>
          <a href="../../change-log.html" class="nav-link">Change Log</a>
          <a href="https://discordapp.com/invite/RYqkVuw" class="nav-link">Get Help</a>
        </nav>
      </div>
    </footer>

    <script src="<?php echo e(asset('vendor/dashforge/lib/jquery/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('vendor/dashforge/lib/bootstrap/js/bootstrap.bundle.min.js')); ?>"></script>
    <script src="<?php echo e(asset('vendor/dashforge/lib/feather-icons/feather.min.js')); ?>"></script>
    <script src="<?php echo e(asset('vendor/dashforge/lib/perfect-scrollbar/perfect-scrollbar.min.js')); ?>"></script>

    <script src="<?php echo e(asset('vendor/dashforge/assets/js/dashforge.js')); ?>"></script>

    <!-- append theme customizer -->
    <script src="<?php echo e(asset('vendor/dashforge/lib/js-cookie/js.cookie.js')); ?>"></script>
    
  </body>
</html>
<?php /**PATH /Users/choirulanam/php/nusahub/LaravelCMSAPI/resources/views/auth/dashforge-templates/register.blade.php ENDPATH**/ ?>