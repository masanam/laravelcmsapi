

<ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="id-tab" data-toggle="tab" href="#idtab" role="tab" aria-controls="idtab" aria-selected="true">Indonesia</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="en-tab" data-toggle="tab" href="#entab" role="tab" aria-controls="entab" aria-selected="false">English</a>
  </li>
</ul>

<div class="tab-content bd bd-gray-300 bd-t-0 pd-20" id="myTabContent">
  <div class="tab-pane fade show active" id="idtab" role="tabpanel" aria-labelledby="id-tab">
    <!-- tab1 -->
            <!-- Title Field -->
            <div class="form-group col-sm-6">
                <?php echo Form::label('title', 'Title:', ['class' => 'd-block']); ?>

                <?php echo Form::text('title', null, ['class' => 'form-control']); ?>

            </div>

            <!-- Content Field -->
            <div class="form-group col-sm-12 col-lg-12">
                <?php echo Form::label('content', 'Description:', ['class' => 'd-block']); ?>

                <textarea name="content" class="form-control my-editor"><?php echo old('content', isset($release) ? $release->content : ''); ?></textarea>
            </div>
            </div>
    <div class="tab-pane fade show active" id="entab" role="tabpanel" aria-labelledby="en-tab">
    <!-- tab1 -->
        <!-- Title Field -->
        <div class="form-group col-sm-6">
        <?php echo Form::label('title_en', 'Title [EN]:', ['class' => 'd-block']); ?>

            <?php echo Form::text('title_en', null, ['class' => 'form-control']); ?>

        </div>

        <!-- Content Field -->
        <div class="form-group col-sm-12 col-lg-12">
            <?php echo Form::label('content_en', 'Content [EN]:', ['class' => 'd-block']); ?>

            <textarea name="content_en" class="form-control my-editor"><?php echo old('content_en', isset($release) ? $release->content_en : ''); ?></textarea>
        </div>
        </div>
</div>    
<br/>  
<!-- Status Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('category_id', 'Category:'); ?>

    <?php echo Form::select('category_id', $varian->pluck('title', 'id'), null, ['class' => 'form-control select2']); ?>

</div>

 <!-- Picture Field -->     
 <div class="form-group col-sm-12 col-lg-12">
 <?php echo Form::label('picture', 'Picture:'); ?>

    <div id="picture-thumb">
    <img id="holder" src="<?php echo e(isset($release) ? $release->picture : ''); ?>" style="padding:10px;max-width:600px;max-height:300px">
    </div>
	<div class="input-group">
    <input class="form-control" type="text" id="picture" name="picture" value="<?php echo e(old('picture', isset($release) ? $release->picture : '')); ?>">
   <span class="input-group-append">
     <a id="lfm" data-input="picture" data-preview="holder" class="btn btn-primary text-white">
        <i class="fa fa-file"></i> Choose
     </a>
     </span>
   </div>
 </div>
 <p class="tx-pink">* file jpg/jpeg/png, Ukuran : 690px x 460px</p>

<!-- Picture Description Field -->
<!-- <div class="form-group col-sm-6">
    <?php echo Form::label('picture_description', 'Picture Description:', ['class' => 'd-block']); ?>

    <?php echo Form::text('picture_description', null, ['class' => 'form-control']); ?>

</div> -->


<!-- Tag Field -->
<!-- <div class="form-group col-sm-6">
    <?php echo Form::label('tag', 'Tags:', ['class' => 'd-block']); ?>

    <?php echo Form::text('tag', null, ['class' => 'form-control', 'data-role' => 'tagsinput']); ?>

    <p class="tx-pink">* separate with comma</p>

</div> -->

<!-- <?php if(isset($content)): ?>
    <div class="form-group col-sm-6">
        <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" value="1" name="headline" id="customCheck1" <?php echo e($content->headline == '1' ? 'checked' : ''); ?>>
        <label class="custom-control-label" for="customCheck1">Headline</label>
        </div>
    </div>
<?php else: ?>
    <div class="form-group col-sm-6">
        <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" value="1" name="headline" id="customCheck1">
        <label class="custom-control-label" for="customCheck1">Headline</label>
        </div>
</div>
<?php endif; ?> -->

<!-- Status Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('status', 'Status:'); ?>

    <?php echo Form::select('status', $status->pluck('title', 'id'), null, ['class' => 'form-control select2']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('admin.releases.index'); ?>" class="btn btn-light">Cancel</a>
</div>

<?php $__env->startSection('scripts'); ?>
<!-- Relational Form table -->
<script src="<?php echo e(asset('css/select2/js/select2.min.js')); ?>"></script>
<script>
        $('.select2').select2({
          placeholder: 'Choose one',
          searchInputPlaceholder: 'Search options'
        });
        $('.btn-add-related').on('click', function() {
        var relation = $(this).data('relation');
        var index = $(this).parents('.panel').find('tbody tr').length - 1;


        if($('.empty-data').length) {
            $('.empty-data').hide();
        }

        // TODO: edit these related input fields (input type, option and default value)
        var inputForm = '';
        var fields = $(this).data('fields').split(',');
        // $.each(fields, function(idx, field) {
        //     inputForm += `
        //         <td class="form-group">
        //             <?php echo Form::select('`+relation+`[`+relation+index+`][`+field+`]', [], null, ['class' => 'form-control select2', 'style' => 'width:100%']); ?>

        //         </td>
        //     `;
        // })
        $.each(fields, function(idx, field) {
            inputForm += `
                <td class="form-group">
                    <?php echo Form::text('`+relation+`[`+relation+index+`][`+field+`]', null, ['class' => 'form-control', 'style' => 'width:100%']); ?>

                </td>
            `;
        })

        var relatedForm = `
            <tr id="`+relation+index+`">
                `+inputForm+`
                <td class="form-group" style="text-align:right">
                    <button type="button" class="btn-delete btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></button>
                </td>
            </tr>
        `;

        $(this).parents('.panel').find('tbody').append(relatedForm);

        $('#'+relation+index+' .select2').select2();
    });

    $(document).on('click', '.btn-delete', function() {
        var actionDelete = confirm('Are you sure?');
        if(actionDelete) {
            var dom;
            var id = $(this).data('id');
            var relation = $(this).data('relation');

            if(id) {
                dom = `<input class="`+relation+`-delete" type="hidden" name="`+relation+`-delete[]" value="` + id + `">`;
                $(this).parents('.box-body').append(dom);
            }

            $(this).parents('tr').remove();

            if(!$('tbody tr').length) {
                $('.empty-data').show();
            }
        }
    });
</script>
<!-- End Relational Form table -->


<?php $__env->stopSection(); ?>
<?php /**PATH /Users/choirulanam/php/nusahub/LaravelCMSAPI/resources/views/admin/releases/fields.blade.php ENDPATH**/ ?>