<?php echo Form::open(['route' => ['admin.users.destroy', $id], 'method' => 'delete']); ?>


    <a href="<?php echo e(route('admin.users.show', $id)); ?>" class='btn btn-outline-secondary btn-xs btn-icon'>
        <i class="fa fa-eye"></i>
    </a>
    <a href="<?php echo e(route('admin.users.edit', $id)); ?>" class='btn btn-outline-primary btn-xs btn-icon'>
        <i class="fa fa-edit"></i>
    </a>
    <?php echo Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-outline-danger btn-xs btn-icon',
        'onclick' => "return confirm('Are you sure?')"
    ]); ?>


<?php echo Form::close(); ?>

<?php /**PATH /Users/choirulanam/php/nusahub/LaravelCMSAPI/resources/views/admin/users/datatables_actions.blade.php ENDPATH**/ ?>